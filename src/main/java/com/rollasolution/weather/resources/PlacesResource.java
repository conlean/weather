package com.rollasolution.weather.resources;

import org.springframework.stereotype.Component;
import twitter4j.Trends;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/places")
@Component
public class PlacesResource {

	@GET
	@Path("/trends/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPlaces(@PathParam("id") String id) {
		
		try {
            // The factory instance is re-useable and thread safe.
            Twitter twitter = TwitterFactory.getSingleton();

			Trends trends = twitter.getPlaceTrends(Integer.parseInt(id));
			return Response.status(Response.Status.ACCEPTED).entity(trends).build();
			
		} catch (NumberFormatException e) {
			e.printStackTrace();
			return Response.status(Response.Status.NOT_ACCEPTABLE).build();
		
		} catch (TwitterException e) {
			e.printStackTrace();
			return Response.status(e.getStatusCode()).entity(e.getMessage()).build();
		}
		
	}
	
}
