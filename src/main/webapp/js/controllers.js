'use strict';

/* Controllers */
var weatherControllers = angular.module('weatherControllers', ['ngResource']);



/**
 * Home Controller
 */
weatherControllers.controller('HomeController', ['$scope', '$http', 'HomeService',
    function ($scope, $http, HomeService) {

        $scope.selected = undefined;
        $scope.lon = -77.03;
        $scope.lat = 38.89;
        $scope.mapError = null;
        $scope.woeid = null;
        $scope.cityWeather = null;
        $scope.showWeatherDiv = false;
        $scope.map = null;
        $scope.trends = null;
        $scope.units = 'imperial';

        $scope.getCountries = function(val) {

            return HomeService.getCountries(val)
                .then(function(res){
                    return res.data;
                });
        };

        $scope.onSelection = function (item) {
            console.log("Selected item: " + item.capital);
            $scope.selected = item;
            $scope.getWeather(item);
            $scope.getLocalTrendTopics(item);
        };

        $scope.getWeather = function(item) {

            return HomeService.getWeather(item.capital, item.name, $scope.units )
            .then(function(res){
                $scope.mapError = null;
               if(res.data.cod != '200'){
                   $scope.mapError = res.data.message;
               }else{
                   $scope.showWeatherDiv = true;
                   $scope.cityWeather = res.data;
                   $scope.lat = res.data.coord.lat;
                   $scope.lon = res.data.coord.lon;
                   $scope.centerOnCapital();
                   $scope.getLocalTrendTopics(item);
               }
                 console.log( 'weather: ' + res);
            });
        };

        //center the map over the capital and zooms in
        $scope.centerOnCapital = function(){
            var centre = new OpenLayers.LonLat($scope.lon, $scope.lat);
            centre.transform(
                new OpenLayers.Projection("EPSG:4326"),
                new OpenLayers.Projection("EPSG:900913")
            );
            $scope.map.setCenter( centre, 8);
        }

        //initiates the map
        $scope.initMap = function () {

            $scope.map = null;
            console.log('latlon: ' +$scope.lat + ', ' + $scope.lon);

            var lonlat = new OpenLayers.LonLat($scope.lon, $scope.lat);

            console.log('lonlat: ' + lonlat);
            $scope.map = new OpenLayers.Map("basicMap",
                {
                    units: 'm',
                    projection: new OpenLayers.Projection("EPSG:900913"),
                    displayProjection: new OpenLayers.Projection("EPSG:4326")
                }
            );

            var SRS_GOOGLE = new OpenLayers.Projection("EPSG:900913");
            var SRS_LONLAT = new OpenLayers.Projection("EPSG:4326");
            lonlat.transform(SRS_LONLAT,SRS_GOOGLE);

            // Create overlays
            // map layer OSM
            var mapnik = new OpenLayers.Layer.OSM();
            // Create weather layer
            var stations = new OpenLayers.Layer.Vector.OWMStations("Stations informations", {units: ''});

            var city = new OpenLayers.Layer.Vector.OWMWeather("Current weather", {units: ''});

            var cloud = new OpenLayers.Layer.XYZ(
                "clouds",
                "http://${s}.tile.openweathermap.org/map/clouds/${z}/${x}/${y}.png",
                {
                    numZoomLevels: 19,
                    isBaseLayer: false,
                    opacity: 0.7,
                    sphericalMercator: true

                }
            );
            cloud.setVisibility(false);

            var precipitation = new OpenLayers.Layer.XYZ(
                "precipitation",
                "http://${s}.tile.openweathermap.org/map/precipitation/${z}/${x}/${y}.png",
                {
                    numZoomLevels: 19,
                    isBaseLayer: false,
                    opacity: 0.7,
                    sphericalMercator: true
                }
            );
            precipitation.setVisibility(false);
            var pressure = new OpenLayers.Layer.XYZ(
                "Pressure",
                "http://${s}.tile.openweathermap.org/map/pressure_cntr/${z}/${x}/${y}.png",
                {
                    numZoomLevels: 19,
                    isBaseLayer: false,
                    opacity: 0.4,
                    sphericalMercator: true

                }
            );
            pressure.setVisibility(false);

            //connect layers to map
            $scope.map.addLayers([mapnik, stations, city, cloud, precipitation, pressure]);

            // Add Layer switcher
            $scope.map.addControl(new OpenLayers.Control.LayerSwitcher());

            $scope.map.setCenter( lonlat, 4 );
        };

        // Gets de current trend topics in the selected city.
        // It first calls Yahoo's APIs to get the WOEId to call twitter.
        // Once it gets the WOEId, it calls our backend to get the list od trend topics
        $scope.getLocalTrendTopics = function (item) {

            return HomeService.getWoeId(item.capital, item.name).then(function (res) {
                $scope.woeid = null;
                $scope.woeid = res.data.places.place[0].woeid;
                console.log('WOEID: ' + $scope.woeid);

                if ($scope.woeid == null) return;

                //Get trend topics from local Java API
                return HomeService.getTrendTopics($scope.woeid)
                    .then(function (res) {
                        $scope.trends = res.data.trends;
                    })
            })
        };
    }

]);
