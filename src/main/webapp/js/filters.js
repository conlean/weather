'use strict';

/* Filters */

angular.module('myApp.filters', []).
  filter('interpolate', ['version', function(version) {
    return function(text) {
      return String(text).replace(/\%VERSION\%/mg, version);
    }
  }]);

angular.module('weatherFilters', []).filter('weathersFilter', function() {
	return function(products, filters) {
		if (filters == 0)
			return products;
		var arrayToReturn = [];
		for (var i = 0; i < products.length; i++) {
			if (filters.indexOf(products[i].familyId) > -1) {
				arrayToReturn.push(products[i]);
			}
		}

		return arrayToReturn;
	};
});

