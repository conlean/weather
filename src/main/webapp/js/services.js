'use strict';

/* Services */

var weatherServices = angular.module('weatherServices', ['ngResource']);

/**
 * Home Service
 * 
 */
weatherServices.factory('HomeService', ['$resource', '$http', function($resource, $http){
    var COUNTRIES_SVC = 'http://restcountries.eu/rest/v1/name/';
    var TRENDS_SVC_LOCAL = 'http://rollasolution.com:8080/weather/rest/places/trends/';
    var OWM_SVC = 'http://api.openweathermap.org/data/2.5/weather?q=';
    var YAHOO_WOEID_SVC = 'http://where.yahooapis.com/v1/places.q(';

    return {
        getCountries: function (country) {
            return $http({
                method: 'GET',
                url: COUNTRIES_SVC + country
            });
        },
        getWeather: function (capital, country, units) {
            return $http({
                method: 'GET',
                url: OWM_SVC + capital + ',' + name + '&units=' + units
            });
        },
        getWoeId: function (capital, country) {
            var YAHOO_KEY = '6CN1H0jV34GkDDLqZRbWcZMB1PMRcXll19EXRStYyuK2.Gytg8ex3FKiQSBRTxD0';
            return $http({
                method: 'GET',
                url: YAHOO_WOEID_SVC + capital + '%20' + country + ')?appid=' + YAHOO_KEY
            });
        },
        getTrendTopics: function (woeid) {
            return $http({
                method: 'GET',
                url: TRENDS_SVC_LOCAL + woeid
            });
        }
    };
}]);
