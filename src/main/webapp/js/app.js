'use strict';

/**
 * App Definition with modules dependencies.
 * @type {module|*}
 */
var moviesApp = angular.module('moviesApp', [
    'weatherControllers',
    'ui.router',
    'weatherServices',
    'weatherFilters',
    'ui.bootstrap',
    'ngRoute'
]);

/**
 * App routing config.
 */
moviesApp.config([ '$locationProvider', '$httpProvider',
        '$stateProvider', '$urlRouterProvider',
        function($locationProvider, $httpProvider,
                 $stateProvider, $urlRouterProvider) {


            // For any unmatched url, redirect to /home/main by default.
            // if the user is logged, is the main page
            $urlRouterProvider.otherwise("/home");
            //
            // Set up the states
            $stateProvider
                /*.state('login', {
                    url: "/login",
                    templateUrl: 'partials/login.html',
                    controller: 'LoginController',
                    access: {
                        isFree: true
                    }
                })*/
                //State base de la aplicacion una vez logueado. El home solo no puede ser
                //instanciado, por eso abstract. Tiene que tener un contenido como subState.
                .state('home', {
                    url: "/home",
                    templateUrl: "partials/home.html",
                    controller: 'HomeController',
                    access: {
                        isFree: true
                    }
                })

            // Use x-www-form-urlencoded Content-Type
            $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
            $httpProvider.defaults.headers.put['Content-Type'] = 'application/json';

            /**
             * The workhorse; converts an object to x-www-form-urlencoded serialization.
             * @param {Object} obj
             * @return {String}
             */
            var param = function(obj) {
                var query = '', name, value, fullSubName, subName, subValue, innerObj, i;

                for(name in obj) {
                    value = obj[name];

                    if(value instanceof Array) {
                        for(i=0; i<value.length; ++i) {
                            subValue = value[i];
                            fullSubName = name + '[' + i + ']';
                            innerObj = {};
                            innerObj[fullSubName] = subValue;
                            query += param(innerObj) + '&';
                        }
                    }
                    else if(value instanceof Object) {
                        for(subName in value) {
                            subValue = value[subName];
                            fullSubName = name + '[' + subName + ']';
                            innerObj = {};
                            innerObj[fullSubName] = subValue;
                            query += param(innerObj) + '&';
                        }
                    }
                    else if(value !== undefined && value !== null)
                        query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
                }

                return query.length ? query.substr(0, query.length - 1) : query;
            };

            // Override $http service's default transformRequest
            $httpProvider.defaults.transformRequest = [function(data) {
                return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
            }];


        } ]

    ).run(function($rootScope, $http, $location, $state) {

        /* Reset error when a new view is loaded */
        $rootScope.$on('$viewContentLoaded', function() {
            delete $rootScope.error;
        });

        $rootScope.hasRole = function(role) {

            if ($rootScope.user === undefined) {
                return false;
            }

            if ($rootScope.user.roles[role] === undefined) {
                return false;
            }

            return $rootScope.user.roles[role];
        };

        //defaults application background color
        $rootScope.defaultBackgroundColor = 'blue';

        $rootScope.backgroundColor = $rootScope.defaultBackgroundColor;
    });